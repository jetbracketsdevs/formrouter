Template.form_search_session.events({
    'keyup .form_search_session input': function(event) {
        setWhenStopTyping(this.session, event.target.value);
    },
    'click .form_search_session button': function(event) {
        setWhenStopTyping(this.session, event.target.value);
    }
});
