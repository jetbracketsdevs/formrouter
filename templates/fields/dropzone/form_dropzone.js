var dropzone = null

Template.form_dropzone.rendered = function() {
     var options = {
        url: "/upload",
        autoProcessQueue : true,
        maxFilesize: 50,
        addRemoveLinks: true,
        dictResponseError: 'Server not Configured',
        acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
        init:function(){
          var self = this;
          // config
          self.options.addRemoveLinks = true;
          self.options.dictRemoveFile = "Delete";
          //New file added
          self.on("addedfile", function (file) {
            //console.log('new file added ', file);
          });
          // Send file starts
          self.on("sending", function (file) {
            //console.log('upload started', file);
            $('.meter').show();
          });

          // File upload Progress
          self.on("totaluploadprogress", function (progress) {
            //console.log("progress ", progress);
            $('.roller').width(progress + '%');
          });

          self.on("queuecomplete", function (progress) {
            $('.meter').delay(999).slideUp(999);
          });

          self.on('success', function(file, response) {
               var res = JSON.parse(response);
               file.fileNameOnServer = res.files[0].name;
               //console.log(res.files[0].name);
             });

          // On removing file
          self.on("removedfile", function (file) {
            //console.log(file);
          });
        }
      };
     dropzone = new Dropzone('#'+this.data.id, options );
};
