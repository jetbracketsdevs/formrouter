Template.form_range.rendered =  function(){
  var _data = this.data;
  console.log('range field:', _data)
  $('#'+_data.id).ionRangeSlider({
      min: _data.min,
      max: _data.max,
      from: _data.from,
      to: _data.to,
      type: 'double',
      prefix: "$",
      postfix: "K",
      maxPostfix: "+",
      prettify: false,
      grid: false
  });
};
