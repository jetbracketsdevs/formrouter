// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See dictionary-tests.js for an example of importing.
export const name = 'formrouter';

Template.registerHelper('SessionGet', function(session) {
    return Session.get(session);
});
