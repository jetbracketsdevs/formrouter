Template.FormRouterDataBase.onCreated(function(){
   var _this = this;
   if(_this.data.id){
      _this.subscribe('formrouter_fields',{id: _this.data.id},{});
  }else{
      console.error("Error Inside FormRouter onCreated. No id was set to subscribe");
  }
});

Template.FormRouterDataBase.helpers({
   getFormRouterField: function(group, id){
    var _data = this.data;
    var dbProps = FormRouterFields.findOne({group: group, id: id });
    if(dbProps){
        for(var idx in _data) {
            dbProps[idx] = _data[idx];
        }
        return dbProps;
    }else{
      console.error("Error Inside FormRouter Helper. No field found for "+id+" on group "+group);
      return null;
    }
   }
});
