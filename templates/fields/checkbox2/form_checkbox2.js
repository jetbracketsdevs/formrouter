Template.form_checkbox2.rendered = function(){
  $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green'
  });
};

Template.form_checkbox2.destroyed = function(){
  $('.i-checks').iCheck('destroy');
};

Template.form_checkbox2.helpers({
  'isChecked': function(value){
    switch(value){
      case 'true':
        value = 'checked';
      break;
      case 'false':
        value = '';
      break;
      case '1':
        value = 'checked';
      break;
      case '0':
        value = '';
      break;
      case 'checked':
        value = 'checked';
      break;
      default:
        value = '';
      break;
    }
    return value;
  }
});
