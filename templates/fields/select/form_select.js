Template.form_select.events({
  "change select": function(event, template){
    console.log('changed: #'+this.id);
     if(this.session){
       Session.set(this.session, event.target.value);
     }
  }
});

Template.form_select.rendered = function(){
    if(this.data.session){
      Session.set(this.data.session, this.data.value);
    }
}
