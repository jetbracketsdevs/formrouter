Template.form_summernote.rendered = function(){
  $('#'+this.data.id).summernote({
    height: 400,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'italic', 'underline', 'clear']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'hr']],
      ['view', ['fullscreen', 'codeview' ]],
      ['help', ['help']]
    ],
    codemirror: { // codemirror options
       theme: 'monokai',
       lineNumbers: true,
       htmlMode: true
    }
  });
  $('#summernote').summernote('code', this.data.value);
}
