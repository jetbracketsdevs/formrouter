Template.form_tagsinput.rendered = function(){
  var options = _.extend( {}, {preventPost: true}, this.data );
  var name = "";
  if(this.data.id){
    name = "#"+this.data.id;
  }else{
    name = ".tagsinput";
  }
  $(name).tagsinput(options);
  if(this.data.options){
    for (var i = 0; i < this.data.options.length; i++) {
      $(name).tagsinput('add', this.data.options[i].value, {preventPost: true});
    }
  }
}
