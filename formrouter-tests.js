// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by dictionary.js.
import { name as packageName } from "meteor/jetbrackets:formrouter";

// Write your tests here!
// Here is an example.
Tinytest.add('formrouter - example', function (test) {
  test.equal(packageName, "formrouter");
});
