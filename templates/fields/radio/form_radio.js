Template.form_radio.rendered = function(){
    $('.i-checks-radio').iCheck('destroy');
    $('.i-checks-radio').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    });
};


Template.form_radio.helpers({
  'isChecked': function(value){
    switch(value){
      case 'true':
        value = 'checked';
        break;
      case 'false':
        value = '';
        break;
      case '1':
        value = 'checked';
        break;
      case '0':
        value = '';
        break;
      case 'checked':
        value = 'checked';
        break;
      default:
        value = '';
        break;
    }
    return value;
  }
});
