Meteor.startup(function () {
  if (FormRouterFields.find({}).count() < 1) {
    FormRouterFields._ensureIndex({
       "name": 1,
       "template": 1,
       "variable": 1
    }, {unique: true});
  }
});
