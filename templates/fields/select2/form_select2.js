function formatRepo (repo) {
  var markup = '<div class="clearfix">' +
  '<div class="col-sm-12">' + repo.text + '</div></div>';
  return markup;
}

function formatRepoSelection (repo) {
  return repo.text;
}

Template.form_select2.rendered =  function(){
  var _data = this.data;
  _data = _.extend({
                     multiple: true,
                     multipleselect: true,
                  },
                  _data);
  console.log(_data);
  $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
  $('.select2-plugin[name="'+_data.name+'"]').select2({
    multiple: _data.multiple,
    multipleselect: _data.multipleselect
  });
  $('.select2-plugin-remote[name="'+_data.name+'"]').select2({ajax: {
        url: _data.url, // "/api/v2/select2",
        type: 'POST',
        dataType: 'json',
        delay: 1000,
        data: function (params) {
          return {
            q: params.term, // search term
            f: _data.upstream_name,
            p: params.page
          };
        },
        processResults: function (result, params) {
          // parse the results into the format expected by Select2
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data, except to indicate that infinite
          // scrolling can be used
          params.page = params.page || 1;

          return {
            results: result.data,
            pagination: {
              more: (params.page * 30) < result.total_count
            }
          };
        },
        cache: false
      },
      multipleselect: _data.multipleselect,
      multiple: _data.multiple,
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 2,
      templateResult: formatRepo, // omitted for brevity, see the source of this page
      templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

};

Template.form_select2.helpers({
  'isSelected':function(value){
    return this.value == value ? true : false;
  },
   'getOptions':function(value){
     return this.options;
   },
   'isTranslate': function(){
     return this.translate == true ? true : false;
   },
   'isDisabled': function(){
     return this.disabled == true ? true : false;
   }
});

Template.form_select2.events({
  'keyup .select2-search': function(event) {
      setWhenStopTyping(this.session, event.target.value);
  },
  'click .select2-search': function(event) {
      setWhenStopTyping(this.session, event.target.value);
  }
});
