Package.describe({
  name: 'jetbrackets:formrouter',
  version: '0.0.4',
  // Brief, one-line summary of the package.
  summary: 'Package for all fields we use on the application',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/jetbracketsdevs/formrouter',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.4.3');
  api.use('ecmascript');
  api.mainModule('formrouter.js', 'client');
  api.mainModule('server.js', 'server');
  api.addFiles(['collection.js']);
  api.addFiles(['client/startup.js'], 'client');
  api.addFiles(['server/startup.js'], 'server');
  api.addFiles([
                  'templates/fields/checkbox/form_checkbox.html',
                  'templates/fields/checkbox/form_checkbox.js',
                  'templates/fields/checkbox2/form_checkbox2.html',
                  'templates/fields/checkbox2/form_checkbox2.js',
                  'templates/fields/date/form_date.html',
                  'templates/fields/dropzone/form_dropzone.html',
                  'templates/fields/dropzone/form_dropzone.js',
                  'templates/fields/email/form_email.html',
                  'templates/fields/listview_detail/form_listview_detail.html',
                  'templates/fields/listview_detail/form_listview_detail.js',
                  'templates/fields/listview_detail2/form_listview_detail.html',
                  'templates/fields/listview_detail2/form_listview_detail.js',
                  'templates/fields/number/form_number.html',
                  'templates/fields/password/form_password.html',
                  'templates/fields/radio/form_radio.html',
                  'templates/fields/radio/form_radio.js',
                  'templates/fields/search_session/form_search_session.html',
                  'templates/fields/search_session/form_search_session.js',
                  'templates/fields/select/form_select.html',
                  'templates/fields/select/form_select.js',
                  'templates/fields/select2/form_select2.html',
                  'templates/fields/select2/form_select2.js',
                  'templates/fields/summernote/form_summernote.html',
                  'templates/fields/summernote/form_summernote.js',
                  'templates/fields/range/form_range.html',
                  'templates/fields/range/form_range.js',
                  'templates/fields/jsswith/form_jsswith.html',
                  'templates/fields/jsswith/form_jsswith.js',
                  'templates/fields/tagsinput/form_tagsinput.html',
                  'templates/fields/tagsinput/form_tagsinput.js',
                  'templates/fields/tel/form_tel.html',
                  'templates/fields/text/form_text.html',
                  'templates/fields/text/form_text.js',
                  'templates/fields/text_group_icon/form_text_group_icon.html',
                  'templates/fields/text_money/form_money.html',
                  'templates/fields/textarea/form_textarea.html',
                  'templates/FormRouter.html',
                  'templates/FormRouterDataBase.html',
                  'templates/FormRouterDataBase.js'
  ], 'client');
  api.use(['tracker','mongo','session', 'jquery','http', 'blaze-html-templates@1.1.2']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('jetbrackets:formrouter');
  api.mainModule('formrouter-tests.js');
});
